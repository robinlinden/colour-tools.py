# Generate range of colours

def make_tuple(colour):
    """
    Turns colours of the format "#000000" into (0,0,0),
    "#ffffcc" into (255,255,204), or #ff0 into (0,0,255)
    """
    if len(colour) == 4:
        rgb = (int(colour[1] + colour[1], 16),
               int(colour[2] + colour[2], 16),
               int(colour[3] + colour[3], 16))
    elif len(colour) == 7:
        rgb = (int(colour[1:3], 16),
               int(colour[3:5], 16),
               int(colour[5:7], 16))
    else:
        print("Invalid colour code entered. "
              "Use the format \"#abc\" or \"#aabbcc\".")
        raise SystemExit()
    return rgb

def make_hex(rgb):
    """
    Takes an rgb tuple (0,127,126) and returns a hexadecimal colour
    such as '#007f7e'.
    """
    return "#{0[0]:02x}{0[1]:02x}{0[2]:02x}".format(rgb)

def interpolate_tuple(start, goal, steps):
    """
    Takes two RGB colour sets, mixes them over a specified number of
    steps, and returns the list.
    """
    delta = [goal[0] - start[0],
             goal[1] - start[1],
             goal[2] - start[2]]
    
    buffer = []
    for i in range(0, steps +1):
        rgb = [int(start[0] + (delta[0] * i / steps)),
               int(start[1] + (delta[1] * i / steps)),
               int(start[2] + (delta[2] * i / steps))]
        colour = make_hex(rgb)
        buffer.append(colour)

    return buffer

def interpolate_hex(start, goal, steps):
    """
    Wrapper for interpolate_tuple that accepts colours as hexadecimal.
    E.g., "#abcdef", "#001122", "#a1b2c3", or even "#333" or "#aff"
    """
    start_tuple = make_tuple(start)
    goal_tuple = make_tuple(goal)

    return interpolate_tuple(start_tuple, goal_tuple, steps)

def colour_chart(start, end, steps):
    """
    Utility function that generates and prints a list of colours.
    """
    colours = interpolate_hex(start, end, steps)
    print("---\n",start, "->", end, "in", steps, "steps.\n---")
    for colour in colours:
        print(colour)
    print()

# Example: show 7 steps between these two colours.
colour_chart("#999933", "#ffffcc", 7)
# Example: show 3 steps between these two colours.
colour_chart("#abc", "#000", 3)



# Pastelify colour

def pastelify(rgb):
    """
    Turns any hexadecimal colour into a pastel version of itself by taking
    the average of it and (255, 255, 255).
    """
    rgb = make_tuple(rgb)
    rgb = [int((rgb[0] + 255) / 2),
           int((rgb[1] + 255) / 2),
           int((rgb[2] + 255) / 2)]
    return make_hex(rgb)



# Generate random colour

import random
random.seed()

def random_colour():
    """
    Returns a random colour in represented as a tuple.
    """
    rgb = (random.randrange(256),
           random.randrange(256),
           random.randrange(256))
    return rgb

def random_colour_hex():
    """
    Returns a random colour in its hexadecimal representation.
    """
    return make_hex(random_colour())



# Open colour in Hex Preview

import webbrowser

def web_preview(hex_list):
    """
    Opens HexPreview in your default browser to view
    the colours in hex_list.
    """
    webbrowser.open(web_preview_url(hex_list))

def web_preview_url(hex_list):
    """
    Returns a hexpreview url that allows you to view the colours in
    hex_list side-to-side.
    """
    buffer = "http://hexpreview.com/"
    for colour in hex_list:
        buffer = buffer + "(" + colour[1:] + ")"
    return buffer
    
a = random_colour_hex()
print(web_preview_url([a, pastelify(a)]))
